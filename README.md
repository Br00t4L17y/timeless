# Timeless

## TEMPLATE

Name
Description

file:
{
"projectID": #,
"fileID": #,
"required": true
}

---

Adorn
Decorations/Furniture

file: Adorn 1.9.1 for 1.16.1
{
"projectID": 320215,
"fileID": 2990980,
"required": true
}

---

Art of Alchemy
Transmutation of one substance to another.

file: artofalchemy-1.0.0-rc2+1.16.1
{
"projectID": 368395,
"fileID": 2989254,
"required": true
}

---

Bunch o' Trinkets
Useful trinkets to aid movement and building

file: 1.16-bunch-o-trinkets-1.0.1
{
"projectID": 356165,
"fileID": 2988010,
"required": true
}

---

Cave Biomes
Adds over 25 new cave biomes

file: Cave Biomes 0.2.4 for 1.16.1
{
"projectID": 371307,
"fileID": 3015431,
"required": true
}

---

Diagonal Panes
Diagonal panes of glass

file: fabric-diagonal-panes-0.3.0 [1.16.1]
{
"projectID": 339575,
"fileID": 2991788,
"required": true
}

---

Extra Alchemy
Potions

file: extraalchemy-fabric-1.16.1-1.1.0
{
"projectID": 247357,
"fileID": 3019681,
"required": true
}

---

Fabric API
Core library for fabric api

file: [1.16.1] Fabric API 0.16.2 build 385
{
"projectID": 306612,
"fileID": 3018319,
"required": true
}

---

Fabric Language Kotlin
Enables useage of kotlin language for fabric mods

file: Fabric Language Kotlin 1.3.72+build.1
{
"projectID": 308769,
"fileID": 2989695,
"required": true
}

---

Glassential
New types of glass

file: glassential-fabric-1.16.1-1.1.4
{
"projectID": 320778,
"fileID": 2993015,
"required": true
}

---

Gubbins
Added General Features

file: gubbins-1.1.1-1.16.1
{
"projectID": 374432,
"fileID": 2993248,
"required": true
}

---

Hwyla
Tooltip Information

file: Hwyla-fabric-1.16.1-1.9.22-75
{
"projectID": 253449,
"fileID": 2989943,
"required": true
}

---

Inmis
(I Need More Inventory Space) - Backpacks

file: inmis-1.2.0-1.16.1
{
"projectID": 369254,
"fileID": 3010868,
"required": true
}

---

Inventory Sorting
Inventory Management

file: InventorySorter-1.7.0-1.16
{
"projectID": 325471,
"fileID": 3011206,
"required": true
}

---

Lithium (Fabric)
Performance Improvements

file: lithium-fabric-mc1.16.1-0.5.1
{
"projectID": 360438,
"fileID": 3000628,
"required": true
}

---

Mod Menu
GUI Information

file: modmenu-1.14.5+build.30
{
"projectID": 308702,
"fileID": 3006672,
"required": true
}

---

Modern Glass Doors
Furniture

file: glassdoor-1.4.4-1.16
{
"projectID": 326641,
"fileID": 2987178,
"required": true
}

---

Phosphor (Fabric)
Performance Improvements

file: phosphor-fabric-mc1.16.1-0.6.0+build.7
{
"projectID": 372124,
"fileID": 2987621,
"required": true
}

---

Reborn Core
API

file: RebornCore-1.16-4.3.4+build.64
{
"projectID": 237903,
"fileID": 3014349,
"required": true
}

---

Roughly Enough Items (REI)
Item Recipes

file: RoughlyEnoughItems-4.10.3
{
"projectID": 310111,
"fileID": 3013302,
"required": true
}

---

Sodium
Performance Improvements

file: sodium-fabric-mc1.16.1-0.1.0
{
"projectID": 394468,
"fileID": 3003093,
"required": true
}

---

Soulbound (Fabric)
Enchantment

file: Soulbound-fabric-1.16.1-1.1.1-7
{
"projectID": 321602,
"fileID": 2989863,
"required": true
}

---

Tech Reborn
Technology

file: TechReborn-1.16-3.4.5+build.88
{
"projectID": 233564,
"fileID": 3014350,
"required": true
}

---

Terrestria
Biomes

file: terrestria-2.0.4+build.50
{
"projectID": 323974,
"fileID": 3010731,
"required": true
}

---

Trinkets (Fabric)
Additional Equipment API

file: trinkets-2.6.3
{
"projectID": 341284,
"fileID": 3003416,
"required": true
}

---

VanillaDeathChest (Fabric)
Item Loss Prevention

file: vanilladeathchest-1.16.1-1.11.0.5-fabric
{
"projectID": 393000,
"fileID": 3017923,
"required": true
}

---

VoxelMap
Minimap

file: (Fabric) VoxelMap for 1.16(.1)
{
"projectID": 225179,
"fileID": 3017436,
"required": true
}

---

Winged
Upgrades to Elytra

file: winged-1.16-1.2
{
"projectID": 381349,
"fileID": 2992139,
"required": true
}

---
